const Sequelize = require('sequelize');

const sequelize = new Sequelize(
    'calendar',
    'root',
    'tomorro3',
    {
        host: 'localhost',
        dialect: 'mysql',
        pool: {
            max: 5,
            min: 0,
            acquire: 30000,
            idle: 10000
        }
    });

const user = sequelize.define('user', {
    id: {
      type: Sequelize.INTEGER,
      primaryKey: true,
      autoIncrement: true
    },
    username: {
        type: Sequelize.STRING,
        allowNull: false
    },
    password: {
        type: Sequelize.STRING,
        allowNull: false
    }
});

const room = sequelize.define('room', {
    id: {
      type: Sequelize.INTEGER,
      primaryKey: true,
      autoIncrement: true
    },
    name: {
        type: Sequelize.STRING,
        allowNull: false
    }
});

const reservation = sequelize.define('reservation', {
    id: {
      type: Sequelize.INTEGER,
      primaryKey: true,
      autoIncrement: true
    },
    start: {
        type: Sequelize.DATE,
        allowNull: false
    },
    finish: {
        type: Sequelize.DATE,
        allowNull: false
    }
});

sequelize.authenticate()
    .then(() => {
        user.sync()
            .then(() => {
                return user.create({
                    username: "John",
                    password: "johnpassword"
                });
            }).then(() => {
                return room.sync()
                    .then(() => {
                        return room.create({
                            name: "Lab #1"
                        });
                    });
            }).then(() => {
                return reservation.sync()
                    .then(() => {
                        return reservation.create({
                            id_user: 1,
                            id_room: 1,
                            start: '2018-01-12 20:00:00',
                            finish: '2018-01-12 22:00:00'
                        });
                    });
            });
    }).catch(err => {
        console.error('Unable to connect to the database:', err);
    });

reservation.belongsTo(user, { as: 'user', foreignKey: {name: 'id_user', allowNull: false}});
reservation.belongsTo(room, { as: 'room', foreignKey: {name: 'id_room', allowNull: false}});

module.exports = {
    user,
    room,
    reservation
};
