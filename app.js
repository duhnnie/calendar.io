const express = require('express'),
    bodyParser = require('body-parser'),
    moment = require('moment'),
    Op = require('Sequelize').Op,
    DB = require('./db.js');

var app = express(),
    router = express.Router();

app.use(bodyParser.json());
app.use('/', router);

router.get("/", (req, res) => {
    res.end();
});

router.get('/user', (req, res) => {
    DB.user.findAll().then((data) => {
        res.json(data.map(user => {
            let data = user.get();

            delete data.password;

            return data;
        }));
    }).catch(() => {
        res.status(500).json({
            error: 002,
            message: "An error occurred."
        });
    });
});

router.post('/user', (req, res) => {
    DB.user.create({
        username: req.body.username,
        password: req.body.password
    }).then((result) => {
        result = result.get();

        delete result.password;

        res.status(200).json(result);
    }).catch((err) => {
        console.error("error: ", err);
        res.status(500).json({
            error: 001,
            message: "The user wasn't created due an internal error."
        });
    });
});

router.patch('/user/:id', (req, res) => {
    let userId = parseInt(req.params.id),
        body = req.body || {};

    DB.user.update(body, {
        where: {
            id: userId
        }
    }).then((data) => {
        res.json({ updatedRows: data });
    }).catch(() => {
        res.status(500).json({
            error: 003,
            message: "An error has ocurred"
        });
    });
});

router.delete('/user/:id', (req, res) => {
    let userId = parseInt(req.params.id);

    DB.user.destroy({
      where: {
        id: userId
      }
  }).then((data) => {
      res.json({ deletedRows: data });
  }).catch(() => {
      res.status(500).json({
          error: 4,
          message: "An error has ocurred"
      });
  });
});

router.get('/room', (req, res) => {
    DB.room.findAll().then((data) => {
        res.json(data.map(item => {
            return item.get();
        }));
    }).catch(() => {
        res.status(500).json({
            error: 002,
            message: "An error occurred."
        });
    });
});

router.post('/room', (req, res) => {
    DB.room.create({
        name: req.body.name
    }).then((result) => {
        res.status(200).json(result.get());
    }).catch((err) => {
        console.error("error: ", err);
        res.status(500).json({
            error: 002,
            message: "The item wasn't created due an internal error."
        });
    });
});

router.patch('/room/:id', (req, res) => {
    let id = parseInt(req.params.id),
        body = req.body || {};

    DB.room.update(body, {
        where: {
            id: id
        }
    }).then((data) => {
        res.json({ updatedRows: data });
    }).catch(() => {
        res.status(500).json({
            error: 003,
            message: "An error has ocurred"
        });
    });
});

router.delete('/room/:id', (req, res) => {
    let id = parseInt(req.params.id);

    DB.room.destroy({
      where: {
        id: id
      }
  }).then((data) => {
      res.json({ deletedRows: data });
  }).catch(() => {
      res.status(500).json({
          error: 4,
          message: "An error has ocurred"
      });
  });
});

router.get('/room/:room/reservation', (req, res) => {
    let id_room = req.params.room,
        start = req.query && req.query.start ,
        finish = req.query && req.query.finish,
        where = { id_room: req.params.room };

    if (start && finish) {
        where = {
            [Op.or]: {
                start: {
                    [Op.gte]: start,
                    [Op.lt]: finish
                },
                finish: {
                    [Op.gt]: start,
                    [Op.lte]: finish
                }
            },
            id_room: req.params.room
        }
    }

    DB.reservation.findAll({
        where: where
    }).then((data) => {
        res.json(data.map(item => {
            return item.get();
        }));
    }).catch(() => {
        res.status(500).json({
            error: 002,
            message: "An error occurred."
        });
    });
});

router.post('/room/:room/reservation', (req, res) => {
    DB.reservation.create({
        id_room: req.params.room,
        id_user: req.body.id_user,
        start: req.body.start,
        duration: req.body.duration
    }).then((result) => {
        res.status(200).json(result.get());
    }).catch((err) => {
        console.error("error: ", err);
        res.status(500).json({
            error: 002,
            message: "The item wasn't created due an internal error."
        });
    });
});

app.listen(3000, () => {
    console.log('Up and running on port 3000.');
});
